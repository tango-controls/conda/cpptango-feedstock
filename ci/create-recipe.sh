#!/bin/bash

# Clone with full depth to be sure to get the correct distance
git clone --recurse-submodules -b $CPPTANGO_REV https://gitlab.com/tango-controls/cpptango.git
pushd cpptango
describe_parts=($(git describe --tags | sed "s/-/ /g"))
popd

commit_sha="${describe_parts[*]: -1}"
distance="${describe_parts[*]: -2:1}"

# Use cppTango CMakeLists.txt to get the library version
# We keep only the variables from the file and use configure_file
# to replace the version in both build.sh and meta.yaml
grep cmake_minimum_required cpptango/CMakeLists.txt > CMakeLists.txt
# Use NONE to skip compiler check
echo "project(cpptango NONE)" >> CMakeLists.txt
# Needed to get SUFFIX_VERSION
echo "include(cpptango/configure/git.cmake)" >> CMakeLists.txt
echo "git_describe(GIT_DESCRIPTION SUFFIX_VERSION)" >> CMakeLists.txt
# Get other variables
grep "set(" cpptango/CMakeLists.txt >> CMakeLists.txt
# Set our variables
echo "set(CPPTANGO_REV \"$CPPTANGO_REV\")" >> CMakeLists.txt
echo "set(GIT_COMMIT_SHA \"$commit_sha\")" >> CMakeLists.txt
echo "set(GIT_DISTANCE \"$distance\")" >> CMakeLists.txt
echo "configure_file(recipe/build.sh.in recipe/build.sh @ONLY)" >> CMakeLists.txt
echo "configure_file(recipe/meta.yaml.in recipe/meta.yaml @ONLY)" >> CMakeLists.txt
cat CMakeLists.txt
cmake -S . -B build
mv build/recipe/* recipe/
