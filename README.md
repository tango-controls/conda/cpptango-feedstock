# cpptango-feedstock

The official [cppTango] conda packages are built on conda-forge: <https://github.com/conda-forge/cpptango-feedstock>

This repository includes a recipe to automatically build a development version from [cppTango] main branch.
This dev package is published to the [tango-controls](https://anaconda.org/tango-controls/repo) channel.
It allows other projects to test against the cppTango main branch without having to recompile.

CI is triggered on every commit to [cppTango] main branch.

The `LIBRARY_VERSION` and `SO_VERSION` variables are retrieved from the project `CMakeLists.txt` file
to avoid having to update the recipe when the version changes.

The variable `CPPTANGO_REV` is set to `main` by default. It can be changed to build from another branch.
It's possible to build from a tag, but that should be done on conda-forge.

**Keep in mind that the latest build always overwrites the previous one (if `LIBRARY_VERSION` doesn't change).**
**We keep only one build for a specific version.**

## Installing cpptango

To install the current development version of cppTango, use the `tango-controls/label/dev` channel.

```shell
conda install --yes -c tango-controls/label/dev cpptango cpptango-dbg
```

Note that you need `conda-forge` for the dependencies. If it's not in your default channels, pass it as argument:

```shell
conda install --yes -c tango-controls/label/dev -c conda-forge cpptango cpptango-dbg
```

[cppTango]: https://gitlab.com/tango-controls/cppTango
